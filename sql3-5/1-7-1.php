<!DOCTYPE html>
<html>
<head>
	<title>User Information</title>

<style>
	.form {
		width: 340px;
    	margin: 50px auto;
	}
    .form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .form h2 {
        margin: 0 0 15px;
    }
    .btn-submit {
        background-color: #4CAF50;
        border: none;
        color: white;
        padding: 8px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }
    .btn-logout {
        background-color: #FF0000;
        border: none;
        color: white;
        padding: 8px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }
    .btn-users {
        background-color: #4682B4;
        border: none;
        color: white;
        padding: 8px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }
    
</style>

</head>
<body>
    

    <?php
        include '../sql3-5/db.php';

        session_start();

        // For user session
        if (isset($_SESSION['login_user'])) {

        } else {
            header('Location: ../sql3-5/1-13.php');
        }

        $name = $age = $birthday = $address = $email = $contactNum = $uploadedImage = '';

        $errorMsgName = '';
        $errorMsgAge = '';
        $errorMsgBirth = '';
        $errorMsgAddress = '';
        $errorMsgEmail = '';
        $errorMsgContNum = '';
        $errorUpload = '';

        if (isset($_POST['submit'])) {
        	$_SESSION['name'] = $_POST['name'];
        	$_SESSION['age'] = $_POST['hiddenAge'];
        	$_SESSION['birthday'] = $_POST['birthday'];
        	$_SESSION['gender'] = $_POST['gender'];
        	$_SESSION['address'] = $_POST['address'];
        	$_SESSION['email'] = $_POST['email'];
        	$_SESSION['contactNum'] = $_POST['contactNum'];
        	

        	function inputData($data) {
        		$data = trim($data);
        		$data = stripslashes($data);
        		$data = htmlspecialchars($data);
        		return $data;
        	}

            //PHP 1-10 Upload Images
            $target_dir = "userImages/";
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

            $_SESSION['uploadedImage'] = $target_file;

            // Check file size
            if ($_FILES["fileToUpload"]["size"] > 10000) {
                $errorUpload = 'Sorry, your file is too large. Only 10kb file size';
                $uploadOk = 0;
            }

            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                $errorUpload = 'Sorry, your file was not uploaded.';
                // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                    //echo "The file ". htmlspecialchars( basename( $_FILES["fileToUpload"]["name"])). " has been uploaded.";
                } else {
                    $errorUpload = 'Sorry, there was an error uploading your file.';
                }
            }

            //PHP 1-7 Add Validation for user information form
            // Name field
            if (empty($_POST['name'])) {
                $errorMsgName = 'Name field is required.';
            } else {
                $name = inputData($_POST['name']);
                if (!preg_match("/^[a-zA-Z\s]*$/", $name)) {
                    $errorMsgName = 'Only letters and white space allowed';
                }
            }

            // Birthday field
            if (empty($_POST['birthday'])) {
                $errorMsgBirth = 'Birthday field is required.';
            } else {
                $birthday = inputData($_POST['birthday']);
            }

            // Age field
            if (empty($_POST['age'])) {
                $errorMsgAge = 'Age field is empty. Please input your birthday.';
            } else {
                $age = inputData($_POST['hiddenAge']);
            }

            // Address field
            if (empty($_POST['address'])) {
                $errorMsgAddress = 'Address field is required.';
            } else {
                $address = inputData($_POST['address']);
                if (!preg_match('/^[A-Za-z0-9\-\\,.]+$/', $address)) {
                    $errorMsgAddress = 'Please enter address.';
                }
            }

            // Email field
            if (empty($_POST['email'])) {
                $errorMsgEmail = 'Email field is required.';
            } else {
                $email = inputData($_POST['email']);
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $errorMsgEmail = "Invalid email format";
                }
            }

            // Contact Number field
            if (empty($_POST['contactNum'])) {
                $errorMsgContNum = 'Contact Number field is required.';
            } else {
                $contactNum = $_POST['contactNum'];
                if (!preg_match('/^(09|\+639)\d{9}$/', $contactNum)) {
                    $errorMsgContNum = 'Invalid input. Ex. 0912345678 or +63912345678';
                }
            }

            // Image field
            if($_FILES["fileToUpload"]["tmp_name"] == "") {
                $errorUpload = '<br>No file was selected.';
            }

            if (($name != '') && ($_POST['hiddenAge'] != '') && ($birthday != '') && ($_POST['gender'] != '') && ($address != '') && ($email != '') && ($contactNum != '') && ($_FILES["fileToUpload"]["tmp_name"] != '')) {
                header('Location: ../sql3-5/1-7-2.php');
            }
        }

    ?>

    <div class="form">
	    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" enctype="multipart/form-data">
		    <h2 align="center">User Information</h2>
            Name: <br>
            <input type="text" name="name" size="38" value="<?php echo $name; ?>">
            <span class="error" style="color: red"><?php echo $errorMsgName; ?></span><br><br>
            Birthday: 
            <input type="date" name="birthday" id="birthday" oninput="getAge()" value="">
            <span class="error" style="color: red"><?php echo $errorMsgBirth; ?></span><br><br>
            Age:
            <span id="age" class="age" name="calculatedAge"></span><br>
            <span class="error" style="color: red"><?php echo $errorMsgAge; ?></span><br><br>
            <input type="hidden" name="hiddenAge" id="hiddenAge" value="<?php echo $age; ?>">
            Gender: <br>
                <input type="radio" checked="checked" name="gender" value="Male"> 
                <label for="male">Male</label>
                <input type="radio" name="gender" value="Female">
                <label for="female">Female</label>
                <input type="radio" name="gender" value="Other">
                <label for="other">Other</label><br><br>
            Address: <br>
            <input type="text" name="address" size="38" value="<?php echo $address; ?>"> 
            <span class="error" style="color: red"><?php echo $errorMsgAddress; ?></span><br><br>
            Email: <br>
            <input type="email" name="email" size="38" value="<?php echo $email; ?>"> 
            <span class="error" style="color: red"><?php echo $errorMsgEmail; ?></span><br><br>
            Contact Number: <br>
            <input type="text" name="contactNum" size="38" value="<?php echo $contactNum; ?>"> 
            <span class="error" style="color: red"><?php echo $errorMsgContNum; ?></span><br><br>
            Upload User Profile: <br>
            <input type="file" name="fileToUpload" id="fileToUpload" size="38">
            <span class="error" style="color: red"><?php echo $errorUpload; ?></span><br><br>

            <input type="submit" id="submit" name="submit"> <br><br>

            <center>
                <input type="button" onclick="location.href='../sql3-5/1-9.php';" class="btn-users" value="List of Users Info">
            </center><br>

            <label><b>User: </b><?php echo $_SESSION['login_user']; ?></label>
            <input type="button" onclick="location.href='../sql3-5/1-13-2.php';" class="btn-logout" value="Logout" style="float: right">
	    </form>
    </div>

    <script>
        function getAge() {

            var from = document.getElementById('birthday').value;
            var dob = new Date(from);  
                var monthDiff = Date.now() - dob.getTime();  
                var ageDate = new Date(monthDiff);   
                var year = ageDate.getUTCFullYear();  
                var calculatedAge = Math.abs(year - 1970);  
      
                document.getElementById('age').innerHTML = calculatedAge ;
                document.getElementById('hiddenAge').value = calculatedAge ; 
                
        }
    </script>

</body>
</html>