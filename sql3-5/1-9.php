<!DOCTYPE html>
<html>
<head>
	<title>Users Information in Table</title>
</head>

<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
    .btn-logout {
        background-color: #FF0000;
        border: none;
        color: white;
        padding: 8px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }
    .btn-adduser {
        background-color: #4682B4;
        border: none;
        color: white;
        padding: 8px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }
</style>
<body>

	<?php
        include '../sql3-5/db.php';
	    session_start();
	    // For user session
	    if (isset($_SESSION['login_user'])) {

	    } else {
	    	header('Location: ../sql3-5/1-13.php');
	    }

	?>

	<h2 align="center">Users Information</h2>

    <input type="button" onclick="location.href='../sql3-5/1-7-1.php';" class="btn-adduser" value="Add User Info" />
    <input type="button" onclick="location.href='../sql3-5/1-13-2.php';" class="btn-logout" value="Logout" style="float: right;">
    <label style="float: right"><b>User: </b><?php echo $_SESSION['login_user']; ?></label>
    
    <!-- PHP 1-9 Show user information using table tags and PHP 1-11 Show uploaded images in the table -->
	<table>
        <thead>
            <tr bgcolor="sky blue">
                <center>
                    <th>ID</th>
                	<th>USER PROFILE</th>
                	<th>NAME</th>
                    <th>AGE</th>
                    <th>BIRTHDAY</th>
                    <th>GENDER</th>
                    <th>ADDRESS</th>
                    <th>EMAIL</th>
                    <th>CONTACT NUMBER</th>
                </center>
            </tr>
        </thead>
        <tbody>

            <?php

                //if (($handle = fopen("userInformation.csv", "r")) !== FALSE) {

                	// PHP 1-12 For Pagination
                    if (isset($_GET['page'])){
                        $page = $_GET['page'];
                    } else {
                        $page = 1;
                    }

                    $recordLimit = 10;
                    $offset = ($page - 1) * $recordLimit;

                    //$getRows = file('userInformation.csv');
                    //$totalRows = count($getRows);
                     
                    $query = "SELECT * FROM usersinfo";  
                    $result = mysqli_query($conn, $query);  
                    $totalRows = mysqli_num_rows($result); 
                    $totalPages = ceil($totalRows / $recordLimit);

                    $query = "SELECT * FROM usersinfo LIMIT " . $offset . ',' . $recordLimit;  
                    $result = mysqli_query($conn, $query);  

                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                    
            ?>
                        <tr>
                            <td><?php echo $row['id']; ?></td>
                            <td><img src="<?php echo $row['userImage']; ?>"></td>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['age']; ?></td>
                            <td><?php echo $row['birthday']; ?></td>
                            <td><?php echo $row['gender']; ?></td>
                            <td><?php echo $row['address']; ?></td>
                            <td><?php echo $row['email']; ?></td>
                            <td><?php echo $row['contactNum']; ?></td>
                        </tr>
            <?php

                        }
                    } else {
                        echo "No Data Available.";
                    }

                    //fclose($handle);
                //}
            ?>

        </tbody>
    </table>

        <br>
            <center>
            	<?php for ($numberPage = 1; $numberPage <= $totalPages; $numberPage++) { ?>
                    <a href="?page=<?php echo $numberPage; ?>"><?php echo $numberPage; ?></a> 
                <?php } ?> <br><br>
             
            </center>

</body>
</html>