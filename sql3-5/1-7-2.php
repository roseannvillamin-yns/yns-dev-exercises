<!DOCTYPE html>
<html>
<head>
	<title>User Information</title>
</head>

<style>
	.form {
		width: 340px;
    	margin: 50px auto;
	}
    .form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .form h2 {
        margin: 0 0 15px;
    }
    .btn-logout {
        background-color: #FF0000;
        border: none;
        color: white;
        padding: 8px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }
    .btn-view {
        background-color: #4682B4;
        border: none;
        color: white;
        padding: 8px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }
    
</style>

<body>

	<?php
	   include '../sql3-5/db.php';
	   session_start();

	   // For user session
        if (isset($_SESSION['login_user'])) {

        } else {
            header('Location: ../sql3-5/1-13.php');
        }

	   $validName = $_SESSION['name'];
	   $validAge = $_SESSION['age'];
	   $validBirth = $_SESSION['birthday'];
	   $validGender = $_SESSION['gender'];
	   $validAddress = $_SESSION['address'];
	   $validEmail = $_SESSION['email'];
	   $validContNum = $_SESSION['contactNum'];
	   $validImage = $_SESSION['uploadedImage'];

       //PHP 1-8 Store inputted user information into a CSV
	   /*$file = fopen("userInformation.csv", "a");

	       $userInfo = array(
	       	'name'  => $validName,
	       	'age' => $validAge,
	       	'birthday' => $validBirth,
	       	'gender' => $validGender,
	       	'address' => $validAddress,
	       	'email'  => $validEmail,
	       	'contactNum' => $validContNum,
	       	'imageUpload' => $validImage,
	       );

	       fputcsv($file, $userInfo);*/
	    $sql = "INSERT INTO usersinfo (name, age, birthday, gender, address, email, contactNum, userImage) VALUES ('$validName', '$validAge', '$validBirth', '$validGender', '$validAddress', '$validEmail', '$validContNum', '$validImage')";

                if (mysqli_query($conn, $sql)) {
                    echo "<br> Data successfully saved!";
                } else {
                    echo "<br> Data not saved!";
                }
	       $msg = '<label class="text-success">User Information is saved. Want to view?</label>';

	?>

    <div class="form">
    	<form>
	        <h2>User Basic Information</h2>

	        <strong>Name:</strong> 
	        <?php echo $validName; ?> <br>
	        <strong>Age:</strong> 
	        <?php echo $validAge; ?> <br>
	        <strong>Birthday:</strong> 
	        <?php echo $validBirth; ?> <br>
	        <strong>Gender:</strong> 
	        <?php echo $validGender; ?> <br>
	        <strong>Address:</strong> 
	        <?php echo $validAddress; ?> <br>
	        <strong>Email:</strong> 
	        <?php echo $validEmail; ?> <br>
	        <strong>Contact Number:</strong> 
	        <?php echo $validContNum; ?> <br><br>
	        <strong>User Profile:</strong>
	        <img src="<?php echo $validImage; ?>"> <br><br>
	        <?php echo $msg; ?>
	        <input type="button" onclick="location.href='../sql3-5/1-9.php';" class="btn-view" value="Click Here" /><br><br>

	        <label><b>User: </b><?php echo $_SESSION['login_user']; ?></label>
	        <input type="button" onclick="location.href='../sql3-5/1-13-2.php';" class="btn-logout" value="Logout" style="float: right">
            
	    </form>
	</div>

</body>
</html>