<!DOCTYPE html>
<html>
<head>
	<title>Create Account</title>
</head>

<style>
	.create-form {
		width: 340px;
    	margin: 50px auto;
	}
    .create-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .create-form h2 {
        margin: 0 0 15px;
    }
    .button-login {
        background-color: #4CAF50;
        border: none;
        color: white;
        padding: 8px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }
    .button-create {
        background-color: #4682B4;
        border: none;
        color: white;
        padding: 8px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }
    
</style>
<body>

	<?php
        include_once '../sql3-5/db.php';
        session_start();
        $errorMsg = '';
        $errorMsgPass = '';

        if (isset($_POST['submit'])) {
        	if (empty($_POST['username'])) {
        		$errorMsg = 'ID field is required.';
        	} else {
        		if (!preg_match("/^[a-zA-Z-' ]*$/", $_POST['username'])) {
        			$errorMsg = "Only alphanumeric are allowed";
        		}
        	}
        	if (empty($_POST['password'])) {
                $errorMsgPass = 'Password field is required.';
        	} else {
        		if (!preg_match("/^[a-zA-Z-' ]*$/", $_POST['password'])) {
        			$errorMsgPass = "Only alphanumeric are allowed";
        		}
        	}

        	if ($_POST['username'] != '' && $_POST['password'] != '') {

        		$sql = "INSERT INTO users (username, password) VALUES ('".$_POST['username']."', '".$_POST['password']."')";

                if (mysqli_query($conn, $sql)) {
                    $msg = "<br> New account created!";
                    header('Location: ../sql3-5/1-13.php');
                } else {
                    $msg = "<br> Failed to create account!";
                }
        	}
        }
    ?>

    <div class="create-form">
	   <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" >
            <h2 align="center">Create Account</h2>
   	        <label for="username"><b>Username:</b></label>
	        <input type="text" name="username" size="38"> <br>
	        <span class="error" style="color: red"><?php echo $errorMsg; ?></span><br><br>
	        <label for="password"><b>Password:</b></label>
	        <input type="password" name="password"size="38"> <br>
	        <span class="error" style="color: red"><?php echo $errorMsgPass; ?></span><br>
            
            <center>
                <button type="submit" name="submit" class="button-create">Create</button> <br><br>
                <input type="button" onclick="location.href='../sql3-5/1-13.php';" class="button-login" value="Login">
            </center>
       </form>
    </div>

</body>
</html>