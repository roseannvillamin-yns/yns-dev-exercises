<!DOCTYPE html>
<html>
<head>
	<title>User Login</title>
</head>

<style>
	.login-form {
		width: 340px;
    	margin: 50px auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .button-login {
        background-color: #4CAF50;
        border: none;
        color: white;
        padding: 8px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }
    .button-create {
        background-color: #4682B4;
        border: none;
        color: white;
        padding: 8px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }
    
</style>
<body>

	<?php
        include '../sql3-5/db.php';
        session_start();
        $errorMsg = '';
        $errorMsgPass = '';

        if (isset($_POST['submit'])) {
        	if (empty($_POST['username'])) {
        		$errorMsg = 'Username field is required.';
        	} else {
        		if (!preg_match("/^[a-zA-Z-' ]*$/", $_POST['username'])) {
        			$errorMsg = "Only alphanumeric are allowed";
        		}
        	}
        	if (empty($_POST['password'])) {
                $errorMsgPass = 'Password field is required.';
        	} else {
        		if (!preg_match("/^[a-zA-Z-' ]*$/", $_POST['password'])) {
        			$errorMsgPass = "Only alphanumeric are allowed";
        		}
        	}

        	//$_SESSION['username'] = $_POST['username'];;
        	//$_SESSION['password'] = $_POST['password'];

        	/*if ($_SESSION['username'] != '' && $_SESSION['password'] != '') {
        		header('Location: ../sql3-5/1-7-1.php');
        	}*/

            $username = mysqli_real_escape_string($conn, $_POST['username']);
            $password = mysqli_real_escape_string($conn, $_POST['password']); 

            if ($username != '' && $password != '') {
                $sql = "SELECT id FROM users WHERE username = '$username' and password = '$password'";
                $result = mysqli_query($conn, $sql);
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
      
                $count = mysqli_num_rows($result);
        
                if($count == 1) {
                    $_SESSION['login_user'] = $username;
                    header('location: ../sql3-5/1-7-1.php');
                } else {
                    echo $error = "<br> Your Login Name or Password is incorrect!";
                }
            }
        }
    ?>

    <div class="login-form">
	   <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" >
            <h2 align="center">Login Page</h2>
   	        <label for="username"><b>Username:</b></label>
	        <input type="text" name="username" size="38"> <br>
	        <span class="error" style="color: red"><?php echo $errorMsg; ?></span><br><br>
	        <label for="password"><b>Password:</b></label>
	        <input type="password" name="password"size="38"> <br>
	        <span class="error" style="color: red"><?php echo $errorMsgPass; ?></span><br>
            
            <center>
                <button type="submit" name="submit" class="button-login">Login</button> <br><br>
                <input type="button" onclick="location.href='../sql3-5/1-13-3.php';" class="button-create" value="Create Account">
            </center>
       </form>
    </div>

</body>
</html>