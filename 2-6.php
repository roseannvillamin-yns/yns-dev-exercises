<!DOCTYPE html>
<html>
<head>
	<title>Javascript - Label below the Button</title>
</head>
<body>

	<h2>Javascript - Label Below the Button</h2>

	<button onclick="appendLabel()">Click Here</button> 
	<p id="output"></p>

    <script>
	
        function appendLabel() { 
            document.getElementById("output").innerHTML +=  
              "<h3>Hello Javascript!</h3>"; 
        } 

</script>

</body>
</html>