<!DOCTYPE html>
<html>
<head>
    <title>Users Information in Table</title>
</head>

<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>
<body>

    <?php
        session_start();
        // For user session
        if (isset($_SESSION['id'])) {

        } else {
            header('Location: 1-13.php');
        }

    ?>

    <h2 align="center">Users Information</h2>


    
    <!-- PHP 1-9 Show user information using table tags and PHP 1-11 Show uploaded images in the table -->
    <table>
        <thead>
            <tr bgcolor="sky blue">
                <center>
                    <th>USER PROFILE</th>
                    <th>NAME</th>
                    <th>AGE</th>
                    <th>BIRTHDAY</th>
                    <th>GENDER</th>
                    <th>ADDRESS</th>
                    <th>EMAIL</th>
                    <th>CONTACT NUMBER</th>
                </center>
            </tr>
        </thead>
        <tbody>

            <?php

                if (($handle = fopen("userInformation.csv", "r")) !== FALSE) {

                    // PHP 1-12 For Pagination
                    if (isset($_GET['page'])){
                        $page = $_GET['page'];
                    } else {
                        $page = 1;
                    }

                    $recordLimit = 10;
                    $offset = ($page - 1) * $recordLimit;

                    $getRows = file('userInformation.csv');
                    $totalRows = count($getRows);
                    $totalPages = ceil($totalRows / $recordLimit); 

                    
                    $data = array();
                    while (($rows = fgetcsv($handle)) !== FALSE) {
                        array_push($data, $rows);
                    }

                        $data = array_slice($data, $offset, $recordLimit);
                        foreach ($data as $row) {
            ?>
                            <tr>
                                <td><img src="<?php echo $row[7]; ?>"></td>
                                <td><?php echo $row[0]; ?></td>
                                <td><?php echo $row[1]; ?></td>
                                <td><?php echo $row[2]; ?></td>
                                <td><?php echo $row[3]; ?></td>
                                <td><?php echo $row[4]; ?></td>
                                <td><?php echo $row[5]; ?></td>
                                <td><?php echo $row[6]; ?></td>
                            </tr>
            <?php
                        }
                        
                    fclose($handle);
                }
            ?>

        </tbody>
    </table>

    

        <br>
            <center>
                <?php for ($numberPage = 1; $numberPage <= $totalPages; $numberPage++) { ?>
                    <a href="?page=<?php echo $numberPage; ?>"><?php echo $numberPage; ?></a> 
                <?php } ?> <br><br>
             
                <input type="button" onclick="location.href='1-7-1.php';" value="Add User Info" />
                <input type="button" onclick="location.href='1-13-2.php';" value="Logout">
            </center>

</body>
</html>