<!DOCTYPE html>
<html>
<head>
	<title>Javascript - Change Image</title>
</head>
<body>

	<h2>Javascript - Change Image</h2>

	<p>Select Tourist Spot:</p>
    <select id="mySelect" onchange="setImage(this)">
    	<option id="eiffel" value="https://lp-cms-production.imgix.net/image_browser/Effiel%20Tower%20-%20Paris%20Highlights.jpg?auto=format&fit=crop&sharp=10&vib=20&ixlib=react-8.6.4&w=850">Eiffel Tower</option>
    	<option id="jeju" value="https://lh3.googleusercontent.com/bpJUZ_u9Pnyf7r_JVPFpEUB6HZuttWN7ylVFWX7j0ke8cWQLh-NnCBZzUOzj6FXw">Jeju Island</option>
    	<option id="fuji" value="https://www.planetware.com/photos-large/JPN/japan-mt-fuji-and-cherry-blossoms.jpg">Mt. Fuji</option>
    </select> <br><br>
    <img src="" name="imageSwap" /> 

    <script>
    	function setImage(select){
    		var image = document.getElementsByName("imageSwap")[0];
    		image.src = select.options[select.selectedIndex].value;
    	}
    </script>

</body>
</html>