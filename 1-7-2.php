<!DOCTYPE html>
<html>
<head>
	<title>User Information</title>
</head>

<style>
	.form {
		width: 340px;
    	margin: 50px auto;
	}
    .form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .form h2 {
        margin: 0 0 15px;
    }
    
</style>

<body>

	<?php
	   session_start();

	   // For user session
        if (isset($_SESSION['id'])) {

        } else {
            header('Location: 1-13.php');
        }

	   $validName = $_SESSION['name'];
	   $validAge = $_SESSION['age'];
	   $validBirth = $_SESSION['birthday'];
	   $validGender = $_SESSION['gender'];
	   $validAddress = $_SESSION['address'];
	   $validEmail = $_SESSION['email'];
	   $validContNum = $_SESSION['contactNum'];
	   $validImage = $_SESSION['uploadedImage'];

       //PHP 1-8 Store inputted user information into a CSV
	   $file = fopen("userInformation.csv", "a");

	       $userInfo = array(
	       	'name'  => $validName,
	       	'age' => $validAge,
	       	'birthday' => $validBirth,
	       	'gender' => $validGender,
	       	'address' => $validAddress,
	       	'email'  => $validEmail,
	       	'contactNum' => $validContNum,
	       	'imageUpload' => $validImage,
	       );

	       fputcsv($file, $userInfo);
	       $msg = '<label class="text-success">User Information saved into CSV file. Want to view?</label>';

	?>

    <div class="form">
    	<form>
	        <h2>User Basic Information</h2>

	        <strong>Name:</strong> 
	        <?php echo $validName; ?> <br>
	        <strong>Age:</strong> 
	        <?php echo $validAge; ?> <br>
	        <strong>Birthday:</strong> 
	        <?php echo $validBirth; ?> <br>
	        <strong>Gender:</strong> 
	        <?php echo $validGender; ?> <br>
	        <strong>Address:</strong> 
	        <?php echo $validAddress; ?> <br>
	        <strong>Email:</strong> 
	        <?php echo $validEmail; ?> <br>
	        <strong>Contact Number:</strong> 
	        <?php echo $validContNum; ?> <br><br>
	        <strong>User Profile:</strong>
	        <img src="<?php echo $validImage; ?>"> <br><br>
	        <?php echo $msg; ?>
	        <input type="button" onclick="location.href='1-9.php';" value="Click Here" /><br><br>

	        <center>
	        	<input type="button" onclick="location.href='1-13-2.php';" value="Logout">
            </center>

	    </form>
	</div>

</body>
</html>