<!DOCTYPE html>
<html>
<head>
	<title>Javascript - Image Sizes</title>
</head>
<body>

	<h2>Javascript - Image Sizes</h2>

    <img src="https://image.freepik.com/free-vector/male-female-icon-set-man-woman-user-avatar_163786-396.jpg" id="myImage"> <br>
	<button onclick="smallFunction()">Small</button>
	<button onclick="mediumFunction()">Medium</button>
	<button onclick="largeFunction()">Large</button>
	<p id="result"></p>

<script>
    function smallFunction() {
      var imageSize = document.getElementById('myImage');
      imageSize.height = 150;
      imageSize.width = 150;
    }

    function mediumFunction() {
    	var imageSize = document.getElementById('myImage');
    	imageSize.height = 600;
    	imageSize.width =600;
    }

    function largeFunction() {
    	var imageSize = document.getElementById('myImage');
    	imageSize.height = 900;
    	imageSize.width = 900;
    }

</script>

</body>
</html>