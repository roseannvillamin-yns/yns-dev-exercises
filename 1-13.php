<!DOCTYPE html>
<html>
<head>
	<title>User Login</title>
</head>

<style>
	.login-form {
		width: 340px;
    	margin: 50px auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    
</style>
<body>

	<?php
        session_start();
        $errorMsg = '';
        $errorMsgPass = '';

        if (isset($_POST['submit'])) {
        	if (empty($_POST['id'])) {
        		$errorMsg = 'ID field is required.';
        	} else {
        		if (!preg_match("/^[a-zA-Z-' ]*$/", $_POST['id'])) {
        			$errorMsg = "Only alphanumeric are allowed";
        		}
        	}
        	if (empty($_POST['password'])) {
                $errorMsgPass = 'Password field is required.';
        	} else {
        		if (!preg_match("/^[a-zA-Z-' ]*$/", $_POST['password'])) {
        			$errorMsgPass = "Only alphanumeric are allowed";
        		}
        	}

        	$_SESSION['id'] = $_POST['id'];;
        	$_SESSION['password'] = $_POST['password'];

        	if ($_SESSION['id'] != '' && $_SESSION['password'] != '') {
        		header('Location: 1-7-1.php');
        	}
        }
    ?>

    <div class="login-form">
	   <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" >
            <h2 align="center">Login Page</h2>
   	        <label for="id"><b>ID:</b></label>
	        <input type="text" name="id" size="38"> <br>
	        <span class="error" style="color: red"><?php echo $errorMsg; ?></span><br><br>
	        <label for="password"><b>Password:</b></label>
	        <input type="password" name="password"size="38"> <br>
	        <span class="error" style="color: red"><?php echo $errorMsgPass; ?></span><br><br>

	        <button type="submit" name="submit">Login</button>
       </form>
    </div>

</body>
</html>