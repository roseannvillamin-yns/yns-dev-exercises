<!DOCTYPE html>
<html>
<head>
	<title>Javascript - Animation</title>

</head>
<body id="background">


    <script>
    	var bg = document.getElementById("background");
        var color = ["#0000A0", "#ADD8E6"];
        var i = 0;
        
        function change() {
            bg.style.backgroundColor = color[i];
            i++;
  
            if(i > color.length - 1) {
                i = 0;
            }
        }
        
        setInterval(change, 1000);

    </script>


</body>
</html>