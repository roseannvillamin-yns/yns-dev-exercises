<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

    <form method="post">
    	Input number: <input type="number" name="num">
    	<input type="submit" name="submit">
    </form>

    <?php
        if (isset($_POST['submit'])) {
        	$num = $_POST['num'];

        	for($x = 1; $x <= $num; $x++) {
        		if (($x % 3 == 0) && ($x % 5 == 0)) {
        			echo "FizzBuzz";
        		    echo "<br>";

        		} elseif ($x % 5 == 0) {
        			echo "Buzz";
        		    echo "<br>";

        		} elseif ($x % 3 == 0) {
        			echo "Fizz";
        		    echo "<br>";
        		}
        		else {
        			echo $x;
        		    echo "<br>";
        		}
        		
        	}

        }
    ?>

</body>
</html>