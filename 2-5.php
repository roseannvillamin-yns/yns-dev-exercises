<!DOCTYPE html>
<html>
<head>
	<title>Javascript - Characters in Text Box and Label</title>
</head>
<body>

	<h2>Javascript - Characters in Text Box and Label</h2>

    <label>Input Here:</label>
    <input type="text" id="myInput" oninput="myFunction()">

    <p id="result"></p>

<script>
    function myFunction() {
        var words = document.getElementById("myInput").value;
        document.getElementById("result").innerHTML = "You inputted: " + words;
}
</script>

</body>
</html>