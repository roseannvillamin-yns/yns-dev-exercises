<!DOCTYPE html>
<html>
<head>
	<title>Javascript - Hover Image</title>
</head>
<body>

	<h2>Javascript - Hover Image</h2>

	<img src="https://image.freepik.com/free-vector/male-female-icon-set-man-woman-user-avatar_163786-396.jpg" id="images" onmouseover="onHover()" onmouseout="offHover()">
	
	<script>
		function onHover() {
			document.getElementById("images").src = "https://image.freepik.com/free-vector/male-female-face-user-avatar-flat-design-style-vector-icon_68196-491.jpg";
		}

		function offHover(){
			document.getElementById("images").src = "https://image.freepik.com/free-vector/male-female-icon-set-man-woman-user-avatar_163786-396.jpg";
		}
	</script>

</body>
</html>