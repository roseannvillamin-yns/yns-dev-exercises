<!DOCTYPE html>
<html>
<head>
	<title>Navigation Page</title>
	<link rel="stylesheet" href="../5-3/style.css">
</head>

<body>

	<div class="topnav">
		<h1 align="center">Exercises</h1>
	</div>

	<div class="row">
        
        
            <div class="column">
            	<table align="center">
                    <tr>
                        <th><h3>HTML & PHP</h3></th>
                        <th>Links</th>
                    </tr>
                    <tr>
                        <td>Hello World</td>
                        <td><button class="button btn-link" onclick="location.href='../1-1.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>The Four Basic Operations of Arithmetic</td>
                        <td><button class="button btn-link" onclick="location.href='../1-2.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Show the Greatest Common Divisor</td>
                        <td><button class="button btn-link" onclick="location.href='../1-3.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Solve FizzBuzz Problem</td>
                        <td><button class="button btn-link" onclick="location.href='../1-4.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Input Date</td>
                        <td><button class="button btn-link" onclick="location.href='../1-5.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Input User Information</td>
                        <td><button class="button btn-link" onclick="location.href='../1-6-1.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Validation in the User Information Form</td>
                        <td><button class="button btn-link" onclick="location.href='../1-7-1.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Store Inputted User Information into CSV</td>
                        <td><button class="button btn-link" onclick="location.href='../1-7-1.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Show the User Information</td>
                        <td><button class="button btn-link" onclick="location.href='../1-9.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Upload Images</td>
                        <td><button class="button btn-link" onclick="location.href='../1-7-1.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Show Uploaded Images</td>
                        <td><button class="button btn-link" onclick="location.href='../1-9.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Pagination</td>
                        <td><button class="button btn-link" onclick="location.href='../1-9.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Create Login Form</td>
                        <td><button class="button btn-link" onclick="location.href='../1-13.php';">Click</button></td>
                    </tr>

                    <tr>
                        <th><h3>DATABASE</h3></th>
                        <th >Links</th>
                    </tr>
                    <tr>
                        <td>Use database in the applications that you developed</td>
                        <td><button class="button btn-link" onclick="location.href='../sql3-5/1-13.php';">Click</button></td>
                    </tr>
                </table>
            </div>            
            
            <div class="column">
            	<table align="center">
                    <tr>
                        <th><h3>JAVASCRIPT</h3></th>
                        <th>Links</th>
                    </tr>
                    <tr>
                        <td>Show Alert</td>
                        <td><button class="button btn-link" onclick="location.href='../2-1.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Confirm Dialog and Redirection</td>
                        <td><button class="button btn-link" onclick="location.href='../2-2-1.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>The Four Basic Operations of Arithmetic</td>
                        <td><button class="button btn-link" onclick="location.href='../2-3.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Show Prime Numbers</td>
                        <td><button class="button btn-link" onclick="location.href='../2-4.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Input Characters in Text Box and Show it in Label</td>
                        <td><button class="button btn-link" onclick="location.href='../2-5.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Press button and add a label below button</td>
                        <td><button class="button btn-link" onclick="location.href='../2-6.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Show Alert When You Click an Image</td>
                        <td><button class="button btn-link" onclick="location.href='../2-7.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Show Alert When You Click Link</td>
                        <td><button class="button btn-link" onclick="location.href='../2-8.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Change Text and Background color When You Press Buttons</td>
                        <td><button class="button btn-link" onclick="location.href='../2-9.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Scroll Screen When You Press Button</td>
                        <td><button class="button btn-link" onclick="location.href='../2-10.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Change Background Color Using Animation</td>
                        <td><button class="button btn-link" onclick="location.href='../2-11.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Show Image When You Mouse Over an Image</td>
                        <td><button class="button btn-link" onclick="location.href='../2-12.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Change Size of Images When You Press Buttons</td>
                        <td><button class="button btn-link" onclick="location.href='../2-13.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Show Images According to the Options in ComboBox</td>
                        <td><button class="button btn-link" onclick="location.href='../2-14.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Show Current Date and Time</td>
                        <td><button class="button btn-link" onclick="location.href='../2-15.php';">Click</button></td>
                    </tr>
                    <tr>
                        <th><h3>PRACTICE</h3></th>
                        <th>Links</th>
                    </tr>
                    <tr>
                        <td>Quiz with Three Multiple Choices</td>
                        <td><button class="button btn-link" onclick="location.href='../5-1/index.php';">Click</button></td>
                    </tr>
                    <tr>
                        <td>Calendar</td>
                        <td><button class="button btn-link" onclick="location.href='../5-2/index.php';">Click</button></td>
                    </tr>
                </table>
            </div>
    </div>
    


</body>
</html>