<!DOCTYPE html>
<html>
<head>
	<title>Javascript - Prime Numbers</title>
</head>

<style>
    .form {
        width: 340px;
        margin: 50px auto;
    }
    .form form {
        margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .form h2 {
        margin: 0 0 15px;
    }
    
</style>
<body>

    <div class="form">
	    <form method="post">
            <h2 align="center">Prime Numbers</h2>
    	    
            <b>Input number: </b><input type="number" name="num" id="num"> <br><br>
    	    <input type="button" name="submit" onclick="primeFunction()" value="Submit"> <br><br>

            <p><b>Prime Numbers: </b><br>
            <span id="result"></span></p>
        </form>
    </div>

    

    <script>
        function primeFunction() {

            var number = document.getElementById('num').value;
            var result = document.getElementById('result').value;
            document.getElementById("result").innerHTML = "";
            
            for (x = 2; x <= number; x++) { 
                if (isPrimeNumber(x)) { 
                    document.getElementById("result").innerHTML += x + ", ";
                }
            }

            function isPrimeNumber(n) {
                var prime = true;
                for (i = 2; i < n; i++) {
                    if (n % i == 0) {
                        prime = false;
                        break; 
                    }
                }
                return prime;
            }

        }
    </script>

</body>
</html>