<!DOCTYPE html>
<html>
<head>
	<title>Javascript - Colors</title>
</head>
<body>

	<h2>Javascript - Colors</h2>

	<button onclick="blueFunction()">Blue</button>
	<button onclick="redFunction()">Red</button>
	<button onclick="orangeFunction()">Orange</button>
	<button onclick="defaultFunction()">Default</button>
	<p id="result"></p>

<script>
    function blueFunction() {
      document.getElementById("result").innerHTML = "Hi I'm Blue!";
      document.body.style.backgroundColor = "blue";
    }

    function redFunction() {
      document.getElementById("result").innerHTML = "Hi I'm Red!";
      document.body.style.backgroundColor = "red";
    }

    function orangeFunction() {
      document.getElementById("result").innerHTML = "Hi I'm Orange!";
      document.body.style.backgroundColor = "orange";
    }

    function defaultFunction() {
      document.getElementById("result").innerHTML = "Back to Default!";
      document.body.style.backgroundColor = "white";
  }


</script>

</body>
</html>