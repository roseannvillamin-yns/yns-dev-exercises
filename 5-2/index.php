<!DOCTYPE html>
<html>
<head>
	<title>Calendar</title>
	<link rel="stylesheet" href="../5-2/style.css">
</head>
<body>

	<?php

        if (isset($_GET['getMonth'])) {
            $getMonth = $_GET['getMonth'];
        } else {
            $getMonth = date('Y-m');
        }

        $time = strtotime($getMonth . '-01');
 
        // Date Today ex. 2021-03-29
        $today = date('Y-m-d');

        // Day
        $day = date('d', $time);

        // Current Month - 03
        $month = date('m', $time);

        // Month Name - January
        $monthYear = date('F, Y', $time);

        // Current year - 2021
        $year = date('Y', $time);

        // Current Date - Day - Month Date, Year
        $dateToday = date("l - F j, Y");

        // Count of Days in current month
        $daysInMonth = cal_days_in_month(0, $month, $year);

        // Day of the week - 0 for Sunday, 1 for Monday and so on
        $dayOfWeek = date('w', $time);

        // Prev & Next month link
        $prev = date('Y-m', strtotime('-1 month', $time));
        $next = date('Y-m', strtotime('+1 month', $time));

        
        $weeks = array();
        $week = '';

        $week .= str_repeat('<td></td>', $dayOfWeek);

        for ($day = 1; $day <= $daysInMonth; $day++, $dayOfWeek++) {
            $date = $getMonth . '-' . $day;
     
            if ($today == $date) {
                $week .= '<td class="today" style="text-align: center; background-color: yellow;">' . $day;
            } else {
                $week .= '<td>' . $day;
            }

            $week .= '</td>';
     
            if ($dayOfWeek % 7 == 6 || $day == $daysInMonth) {
                if ($day == $daysInMonth) {
                    $week .= str_repeat('<td></td>', 6 - ($dayOfWeek % 7));
                }

                $weeks[] = '<tr>' . $week . '</tr>';
                $week = '';
            }
        }
    ?>

    <div class="container">
        <h1 align="center">Calendar</h1>

        <table align="center">
    	    <center>    	    
    	        <h3><?php echo $dateToday; ?></h3>
    	        <button onclick="location.href='?getMonth=<?= $prev; ?>';" class="btn-getMonth"><</button>
    		    <span><?php echo $monthYear; ?></span>
    		    <button onclick="location.href='?getMonth=<?= $next; ?>';" class="btn-getMonth">></button>
    	    </center>
            <thead>
                <tr>
                    <th>Sun</th>
                    <th>Mon</th>
                    <th>Tues</th>
                    <th>Wed</th>
                    <th>Thu</th>
                    <th>Fri</th>
                    <th>Sat</th>
                </tr>
            </thead>
            <tbody>
                <tr>
        	        <?php
                    
        	            foreach ($weeks as $week) {
                            echo $week;
                        }
                    ?>
                </tr>
            </tbody>
        </table>
    </div>

</body>
</html>