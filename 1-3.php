<!DOCTYPE html>
<html>
<head>
	<title>Greatest Common Divisor</title>
</head>
<body>

    <?php
        $num1 = '';
        $num2 = '';
        $error = '';
        $gcd = '';

        if (isset($_POST['submit'])) {
            $num1 = abs($_POST['num1']);
            $num2 = abs($_POST['num2']);

            if (!empty($num1) & !empty($num2)) {
                while ($num1 != $num2) {
                    if ($num1 > $num2) {
                        $num1 = $num1 - $num2;
                    } else {
                        $num2 = $num2 - $num1;
                    }
                }
                $gcd = $num1;
                $num1 = $num2 = '';

            } else {
                $error = "Please complete the fields. Value/s must be not zero.";
                    
            }
            
        }
    ?>

	<form method="post" style="text-align: center;">
        <h2>Show the Greatest Common Divisor</h2>
		First Number: <input type="number" name="num1" value="<?php echo $num1; ?>">
		Second Number: <input type="number" name=num2 value="<?php echo $num2; ?>">
		<input type="submit" name="submit" value="GCD"><br><br>
        <span style="color: red;"><?php echo $error; ?></span><br><br>
        <span><b><?php echo "GCD: " . $gcd; ?></b></span>
	</form>

</body>
</html>