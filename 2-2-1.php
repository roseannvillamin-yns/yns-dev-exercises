<!DOCTYPE html>
<html>
<head>
	<title>Javascript Confirm Dialog and Redirection</title>
</head>
<body>

    <h2>Javascript Confirm Dialog and Redirection</h2>

    <p>Confirm Dialog and Redirection</p>
	<button onclick="myFunction()">Click Here</button>

    <script>
    	function myFunction() {
    		var dialog = window.confirm("Click to redirect to another page or cancel to remain in this page!");

    		if (dialog) {
    			window.location.href = "2-2-2.php";
    		} else {
    			window.location.href = "2-2-1.php";
    		}
    	}
    </script>

</body>
</html>