<!DOCTYPE html>
<html>
<head>
	<title>Javascript - Current Date and Time</title>
</head>
<body onload = currentDateTime();>

	<h2 align="center">Javascript - Current Date and Time</h2>

	<h4 id="currentDateTime" style="text-align: center;"></h4>


	<script>
		function currentSeconds(){
            var refresh = 1000;
            mytime = setTimeout('currentDateTime()',refresh)
        }

        function currentDateTime() {
            var today = new Date();
		    document.getElementById("currentDateTime").innerHTML = today;
		    currentSeconds();
        }

	</script>

</body>
</html>