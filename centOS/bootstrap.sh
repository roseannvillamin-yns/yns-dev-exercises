# Update Package
sudo yum -y update

# Apache
sudo yum -y install httpd

# Install PHP
sudo yum -y install yum-utils
sudo yum -y install epel-release
sudo yum -y install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
sudo yum-config-manager --enable remi-php72
sudo yum -y install php php-common php-opcache php-mcrypt php-cli php-gd php-curl php-mysql

# Install phpmyadmin
sudo yum -y install phpmyadmin

# Restart Apache
systemctl start httpd.service
systemctl enable httpd.service
