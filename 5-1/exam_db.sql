-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2021 at 07:25 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exam_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `examinees`
--

CREATE TABLE `examinees` (
  `examineeID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `examinees`
--

INSERT INTO `examinees` (`examineeID`, `name`, `score`) VALUES
(1, 'Rose Ann', 10),
(2, 'Rose Ann Villamin', 6);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `questionID` int(11) NOT NULL,
  `question` varchar(255) DEFAULT NULL,
  `choiceA` varchar(255) NOT NULL,
  `choiceB` varchar(255) NOT NULL,
  `choiceC` varchar(255) NOT NULL,
  `answer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`questionID`, `question`, `choiceA`, `choiceB`, `choiceC`, `answer`) VALUES
(1, 'Which of the following is NOT a word used to describe an earthquake?', 'Drop-slide', 'Foreshock', 'Strike-slip', 'Drop-slide'),
(2, 'A doctor with a PhD is a doctor of what?', 'Philosophy', 'Psychology', 'Physical Therapy', 'Philosophy'),
(3, 'What state is the largest state of the United States of America?', 'Alaska', 'California', 'Washington', 'Alaska'),
(4, 'What is considered the rarist form of color blindness?', 'Blue', 'Red', 'Green', 'Blue'),
(5, 'Which ocean borders the west coast of the United States?', 'Pacific', 'Atlantic', 'Arctic', 'Pacific'),
(6, 'What is the fastest land animal?', 'Cheetah', 'Lion', 'Thomson’s Gazelle', 'Cheetah'),
(7, 'Botanically speaking, which of these fruits is NOT a berry?', 'Strawberry', 'Banana', 'Concord Grape', 'Strawberry'),
(8, 'What is the oldest Disney film?', 'Snow White and the Seven Dwarfs', 'Pinocchio', 'Fantasia', 'Snow White and the Seven Dwarfs'),
(9, 'Harvard University is located in which city?', 'Cambridge', 'New York', 'Washington D.C.', 'Cambridge'),
(10, 'What was the first sport to have been played on the moon?', 'Golf', 'Football', 'Tennis', 'Golf');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `examinees`
--
ALTER TABLE `examinees`
  ADD PRIMARY KEY (`examineeID`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`questionID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `examinees`
--
ALTER TABLE `examinees`
  MODIFY `examineeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `questionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
