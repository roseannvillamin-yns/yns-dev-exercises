<!DOCTYPE html>
<html>
<head>
	<title>Examination</title>
</head>

<style>
	.container {
		width: 1000px;
    	margin: 50px auto;
	}
    .container form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .btn-submit {
        background-color: #4682B4;
        border: none;
        color: white;
        padding: 8px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }
    .btn-clear {
        background-color: #4CAF50;
        border: none;
        color: white;
        padding: 8px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }
</style>
<body>

	<?php 
	    include '../5-1/db.php'; 

        session_start();
        if (isset($_POST['submit'])) {
            $_SESSION['examineeName'] = $_POST['examineeName'];
        }

        if (!empty($_SESSION['score'])) {
            $score = $_SESSION['score'];
        } else {
            $score = '';
        }

	?>

	<h1 align="center">EXAMINATION</h1>

    <div class="container">
	    <form name="formExam" method="post" action="../5-1/submit.php">
		    <label><b>Examinee Name: </b></label>
		    <input type="text" name="examineeName" required>
		    <label style="margin-left: 550px"><b>Score: </label>
		    <span id="score" style="color: blue; font-size: 30px;"><?php echo $score; ?></b></span><br>

		    <h4>Questions:</h4>
    
    <?php
        $sql = "SELECT questionID, question, choiceA, choiceB, choiceC, answer FROM questions ORDER BY RAND()";
        $result = mysqli_query($conn, $sql);
        
        for ($quesNum = 1; $quesNum <= 10; $quesNum++) {
        
            while($row = mysqli_fetch_assoc($result)) {
            	if ($quesNum <= 10) {
                
    ?>      
                
                	<label for="question" style="margin-left: 80px;"><?php echo $quesNum .". " . $row['question']; ?></label> <br>
                    <input type="hidden" name="question<?php echo $quesNum; ?>" id="questionID" value="<?php echo $row['questionID']; ?>" required>
                    <input type="radio" name="choice<?php echo $quesNum; ?>" value="<?php echo $row['choiceA']; ?>" style="margin-left: 130px;" required><?php echo $row['choiceA']; ?>
                    <input type="radio" name="choice<?php echo $quesNum; ?>" value="<?php echo $row['choiceB']; ?>" style="margin-left: 130px;" required><?php echo $row['choiceB']; ?>
                    <input type="radio" name="choice<?php echo $quesNum; ?>" value="<?php echo $row['choiceC']; ?>" style="margin-left: 130px;" required><?php echo $row['choiceC']; ?><br><br> 
    <?php       
                }
                $quesNum++;
            }
        }

    ?>
                <center>
                	<button type="submit" name="submit" class="btn-submit" onclick="alert('Thank you for taking the exam!')">Submit</button>
                </center>
	    </form>
    </div>

</body>
</html>