<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form method="post">
		Date: <input type="date" name="dateInput">
		<input type="submit" name="submit">
	</form>

	<?php

        if (isset($_POST['submit'])) {
        	$dateInput = $_POST['dateInput'];

        	echo date('l d-m-y', strtotime($dateInput. ' + 3 day'));
        }

	?>

</body>
</html>