<!DOCTYPE html>
<html>
<head>
	<title>Javascript - Arithmetic Operations</title>
</head>
<body>
    <h2>Arithmetic Operations</h2>

    <form>
	    <label>First Number: </label>
	    <input type="number" id="num1"> 
	    <label>Second Number: </label>
	    <input type="number" id="num2"> <br><br>
	    <label>Select operation:</label>
	        <select id="operation">
	    	    <option value="add">Plus</option>
	    	    <option value="minus">Minus</option>
	    	    <option value="multiply">Multiply</option>
	    	    <option value="divide">Division</option>
	        </select> <br><br>
	    <input type="button" onClick="calculateBy()" name="submit" value="Calculate">
    </form>

        <p>Result: <span id = "result"></span></p>

	<script>
		
		function calculateBy() { 
			firstNum = document.getElementById("num1").value;
			secondNum = document.getElementById("num2").value;
			sign = document.getElementById("operation").value;

			if (sign == 'add') {
			   document.getElementById("result").innerHTML = parseInt(firstNum) + parseInt(secondNum);
			} 
			if (sign == 'minus') {
               document.getElementById("result").innerHTML = parseInt(firstNum) - parseInt(secondNum);
            }
			if (sign == 'multiply') {
				document.getElementById("result").innerHTML = parseInt(firstNum) * parseInt(secondNum);
			}
			if (sign == 'divide') {
				document.getElementById("result").innerHTML = parseInt(firstNum) / parseInt(secondNum);
			}
		}

	</script>

</body>
</html>