<!DOCTYPE html>
<html>
<head>
	<title>Javascript - Scroll Screen</title>

	<style>
        body {
            height: 2500px;
        }
        
        #downBtn {
            position: absolute;
            left: 10%;
            margin-left: -105px;
        }

        #upBtn {
            bottom: 20px;
            left: 10%;
            margin-left: -105px;
            display: block; 
            position: fixed; 
            z-index: 99;
        }
    </style>
</head>
<body>

    
	    <h2>Javascript - Scroll</h2>
	    
	    <button onclick="bottomFunction()" id="downBtn" title="Go to bottom">Go Down</button>
	    <button onclick="topFunction()" id="upBtn" title="Go to top">Go Up</button>
	

    <script>
        function topFunction() {
            window.scrollBy(0, -2500);
        }
        function bottomFunction() {
            window.scrollBy(0, 2500);
        }
    </script>

</body>
</html>