<!DOCTYPE html>
<html>
<head>
	<title>Arithmetic Operations</title>
</head>
<body>

    <form method="post" style="text-align: center;">
        <h2>The Four Basic Arithmetic Operations</h2>
	    First Number: <input type="number" name="num1" step=".01">
	    Second Number: <input type="number" name="num2" step=".01">
	    <br> <br>
        <input type="submit" name="add" value="Addition">
        <input type="submit" name="subtract" value="Subtraction">
        <input type="submit" name="multiply" value="Multiplication">
        <input type="submit" name="divide" value="Division"><br><br>
    </form>

    <?php
        if (isset($_POST['add'])) {
        	$num1 = $_POST['num1'];
        	$num2 = $_POST['num2'];
        	$sum = $num1 + $num2;

        	echo "<center><b>" . " SUM: " . $sum . "</b></center>";
        } elseif (isset($_POST['subtract'])) {
        	
        	$num1 = $_POST['num1'];
        	$num2 = $_POST['num2'];
        	$difference = $num1 - $num2;

        	echo "<center><b>" . "DIFFERENCE: " . $difference . "</b></center>";

        } elseif (isset($_POST['multiply'])) {
        	$num1 = $_POST['num1'];
        	$num2 = $_POST['num2'];
        	$product = $num1 * $num2;

        	echo "<center><b>" . "PRODUCT: " . $product . "</b></center>";

        } elseif (isset($_POST['divide'])) {
        	$num1 = $_POST['num1'];
        	$num2 = $_POST['num2'];

            if ($num2 == 0) {
                echo "<center>" . "Divisor is zero." . "</center>";
            } else {
                echo "<center><b>" . "QUOTIENT: " . $num1 / $num2 . "</b></center>";
            }

        	
        }
    ?>

</body>
</html>
